open Ocamlbuild_plugin

let () = Options.use_ocamlfind := true

let flags_pa_bisect =
  S[A "-syntax"; A"camlp4o";
    A "-package"; A"bisect";
    (* fast mode (but still thread-safe) *)
    A"-ppopt"; P"-mode"; A "-ppopt"; A"fast";
    (* -disable b will disable instrumentation of bindings *)
    A"-ppopt"; A"-disable"; A"-ppopt"; A"b"]

let _ = dispatch begin function
  | After_rules ->
    flag ["ocaml"; "compile"; "bisect"] & flags_pa_bisect;
    flag ["ocaml"; "link"; "bisect"] & flags_pa_bisect;
    flag ["ocaml"; "ocamldep"; "bisect"] & flags_pa_bisect;
  | _ -> ()
end
