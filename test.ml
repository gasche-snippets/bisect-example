let f () = print_endline "f used"
let g () = print_endline "g used"

let () =
  if Random.bool () then f () else g ()
