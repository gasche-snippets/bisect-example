with-ocamlbuild:
	#note that you can also put bisect in the right place
	# in your _tags
	ocamlbuild -tag bisect test.byte

without-ocamlbuild: test.ml
	ocamlfind ocamlc \
                -syntax camlp4o -package bisect \
		-ppopt -mode -ppopt fast \
		-ppopt -disable -ppopt b

clean:
	rm test.cm* test.byte
